package com.example.aramirez.fruitworld;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import model.Fruit;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private GridView gridView;

    private FloatingActionButton floatingActionButton;

    private MyAdapter listAdapter;
    private MyAdapter gridAdapter;

    private int SHOW_LIST_VIEW = 0;
    private int SHOW_GRID_VIEW = 1;

    private int counter = 0;

    private MenuItem menuItemShowGrid;
    private MenuItem menuItemShowList;

    private List<Fruit> fruits;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_my_icon);

        listView = (ListView) findViewById(R.id.listView);
        gridView = (GridView) findViewById(R.id.gridView);

        floatingActionButton = (FloatingActionButton) findViewById(R.id.floatingActionButton);

        this.fruits = getAllFruist();

        listAdapter = new MyAdapter(this, R.layout.fruit_list_item, this.fruits);
        listView.setAdapter(listAdapter);

        gridAdapter = new MyAdapter(this, R.layout.fruit_grid_item, this.fruits);
        gridView.setAdapter(gridAdapter);

        registerForContextMenu(listView);
        registerForContextMenu(gridView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                String origin = fruits.get(index).getOrigin();
                String fruitName = fruits.get(index).getName();
                String message = "The best fruit from " + origin + " is " + fruitName;
                if (origin.equalsIgnoreCase("unknow")) {
                    Toast.makeText(MainActivity.this, "Sorry, we don't have many info about " + fruitName, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                }
            }
        });


        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                String origin = fruits.get(index).getOrigin();
                String fruitName = fruits.get(index).getName();
                String message = "The best fruit from " + origin + " is " + fruitName;
                if (origin.equalsIgnoreCase("unknow")) {
                    Toast.makeText(MainActivity.this, "Sorry, we don't have many info about " + fruitName, Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                }
        }
    });

        floatingActionButton.setOnClickListener(new View.OnClickListener()

    {
        @Override
        public void onClick (View view){
        addFruit();
    }
    });
}


    private List<Fruit> getAllFruist() {
        List<Fruit> list = new ArrayList<Fruit>();
        list.add(new Fruit("Banana", R.mipmap.ic_banana, "Gran Canaria"));
        list.add(new Fruit("Strawberry", R.mipmap.ic_strawberry, "Huelva"));
        list.add(new Fruit("Orange", R.mipmap.ic_orange, "Sevilla"));
        list.add(new Fruit("Apple", R.mipmap.ic_apple, "Madrid"));
        list.add(new Fruit("Cherry", R.mipmap.ic_cherry, "Galicia"));
        list.add(new Fruit("Pear", R.mipmap.ic_pear, "Zaragoza"));
        list.add(new Fruit("Raspberry", R.mipmap.ic_raspberry, "Barcelona"));
        return list;
    }

    public void addFruit() {
        fruits.add(new Fruit("Added n° " + (++counter), R.mipmap.ic_tree, "Unknow"));
        listAdapter.notifyDataSetChanged();
        gridAdapter.notifyDataSetChanged();
    }

    public void changeView(int option) {
        if (option == SHOW_GRID_VIEW) {
            this.menuItemShowGrid.setVisible(false);
            this.menuItemShowList.setVisible(true);
            gridView.setVisibility(View.VISIBLE);
            listView.setVisibility(View.INVISIBLE);
        } else if (option == SHOW_LIST_VIEW) {
            this.menuItemShowList.setVisible(false);
            this.menuItemShowGrid.setVisible(true);
            listView.setVisibility(View.VISIBLE);
            gridView.setVisibility(View.INVISIBLE);
        }
    }

    // Agrega el menu creado al activity
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_menu, menu);
        this.menuItemShowGrid = menu.findItem(R.id.show_grid_view);
        this.menuItemShowList = menu.findItem(R.id.show_list_view);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.show_list_view:
                this.changeView(this.SHOW_LIST_VIEW);
                return true;
            case R.id.show_grid_view:
                this.changeView(this.SHOW_GRID_VIEW);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        menu.setHeaderTitle(fruits.get(info.position).getName());
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        switch (item.getItemId()) {
            case R.id.delete_item:
                this.fruits.remove(info.position);
                listAdapter.notifyDataSetChanged();
                gridAdapter.notifyDataSetChanged();
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}

